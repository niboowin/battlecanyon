﻿using UnityEngine;
using System.Collections;

public class TestGame : MonoBehaviour {

	[SerializeField] Camera gameCam;
	[SerializeField] GameObject groundArea;
	
	
	[SerializeField] GameObject testCannon;
	[SerializeField] GameObject targetAreaReticle;
	[SerializeField] bool bCanPlaceObjects = true;
	
	
	[SerializeField] GameObject[] placedObjects;
	[SerializeField] GameObject[] targetAreaLocations;
	int placedCount = 0;
	
	int gamePhase = 0;
	
	
	[SerializeField] CameraInfo[] cameraPositions;
	
	void Update () {
	
		if(Input.GetMouseButton(0) && bCanPlaceObjects ){
			
			RaycastHit hit;	
			if( Physics.Raycast( gameCam.ScreenPointToRay(Input.mousePosition), out hit  )){
				
				if(hit.collider.gameObject.tag == "ground_area" ){
					bCanPlaceObjects = false;
					
					if(gamePhase == 0){
						PlaceGamePhaseOneObject(hit.point);
					}
					if(gamePhase == 1){
						MarkTaragetAreas( hit.point);
					}					
					
				}	
			}		
		}
	}
	
	
	void PlaceGamePhaseOneObject(Vector3 pos){
		placedObjects[placedCount++] = Instantiate(testCannon, pos, Quaternion.identity ) as GameObject;
		if(placedCount == placedObjects.Length ){
			DoPhaseTwo();
		}else{
			StartCoroutine("placementWait", 0.5f);
		}
	}
	
	
	void MarkTaragetAreas(Vector3 pos){
		
		targetAreaLocations[placedCount++] = Instantiate(targetAreaReticle, pos, Quaternion.identity ) as GameObject;
		if(placedCount == targetAreaLocations.Length ){
			DoPhaseThree();
		}else{
			StartCoroutine("placementWait", 0.5f);
		}	
	}
	
	
	void ExecuteCombat(){
		CheckTargets();		
	}
	
	void CheckTargets(){
		
		foreach(GameObject g in targetAreaLocations){
			float radius = g.GetComponent<SphereCollider>().radius;
			Collider[] hit = Physics.OverlapSphere(g.transform.position, radius);
			foreach(Collider c in hit){
				if(c.gameObject.tag == "enemy_cube"){
					// affected stuff
					c.gameObject.GetComponent<MeshRenderer>().enabled = false;
				}
				
			}
		}

	}
	
	
	IEnumerator placementWait(float t){
		yield return new WaitForSeconds(t);
		bCanPlaceObjects = true;
	}
	
	
	void DoPhaseOne(){
		StartCoroutine(moveCamera(0, 2f) );
	}
	
	
	void DoPhaseTwo(){
		StartCoroutine(moveCamera(1, 2f) );
	}
	
	void DoPhaseThree(){
		StartCoroutine(moveCamera(2, 2f) );
		ExecuteCombat();
	}
	
	
	
	
	
	
	Vector3 lastLookPosition;
	Vector3 curLookPosition;
	
	IEnumerator moveCamera(int infoIndex, float timelen){
		
		float t = 0f;
		CameraInfo info = cameraPositions[infoIndex];
		
		lastLookPosition = cameraPositions[Mathf.Min(infoIndex-1, 0, 3)].position;
		curLookPosition = lastLookPosition;
		
		while(t<timelen){
			
			t+=Time.deltaTime;
			gameCam.transform.position = Vector3.Lerp(gameCam.transform.position, info.position, t/timelen );
			curLookPosition = Vector3.Lerp(lastLookPosition, info.looktarget.position,  t/timelen);
			gameCam.transform.LookAt(curLookPosition);
			
			yield return new WaitForEndOfFrame();
		}
		
		gameCam.transform.position = info.position;
		gameCam.transform.LookAt(info.looktarget.position);
		
		gamePhase = infoIndex;
		placedCount = 0;
		StopCoroutine("placementWait");	
		StartCoroutine("placementWait", 2f);
	}
	
	
}








[System.Serializable]
public class CameraInfo{
	
	public Transform pos;
	public Transform looktarget;
	
	
	public Vector3 position{
		get{ 
			return pos.position;
		}
	}	
}


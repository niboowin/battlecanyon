﻿using UnityEngine;
using System.Collections;

public class BaseCamera : MonoBehaviour {
	
	[SerializeField] Camera gameCam;
	
	// Will use the following for lerp positions.
	// 0 - Base
	// 1 - Left
	// 2 - Right
	[SerializeField] Transform[] camera_positions;

	[SerializeField] Transform tempLookTarget;
	Vector3 targetPos;
	
	public float rate  = 200f;

	void Start () {
		targetPos = camera_positions[0].position;
	}
	
	public void ChangeCamPosition( int i){
		targetPos = camera_positions[i].position;
	}
	
	void Update () {
		
		gameCam.transform.position = Vector3.Lerp( gameCam.transform.position, targetPos, Time.deltaTime * rate );
		
		gameCam.transform.LookAt( tempLookTarget );
	}
}
